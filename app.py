from flask import Flask

UPLOAD_FOLDER = 'upload/'

app = Flask(__name__)
app.secret_key = "3342432dfsdf"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
